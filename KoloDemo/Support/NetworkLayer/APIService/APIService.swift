//
//  APIService.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation

class WebService{
    
    func callWebAPI(_ url:String, param:[String:Any]? = nil, success :@escaping ((Data,Int) -> Void), failure :@escaping ((String) -> Void)){
        if(Reachability.isConnectedToNetwork()){
            guard let url = URL(string: url) else {
                return
            }
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "GET"
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                if data == nil{
                    failure(error.debugDescription)
                }
                else{
                   // let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    if  String(data: data! , encoding: .utf8) != nil {
                        if let httpResponse = response as? HTTPURLResponse {
                            success(data!,httpResponse.statusCode)
                        }
                        
                    }
                    else{
                        failure(error.debugDescription)
                    }
                }
            })
            task.resume()
        }else{
            failure(.kInternetConnectionMsg)
        }
    }
}
