//
//  Swift-MD5.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation
import CryptoKit

extension String {
    var MD5: String {
        let computed = Insecure.MD5.hash(data: self.data(using: .utf8)!)
        return computed.map { String(format: "%02hhx", $0) }.joined()
    }
}
