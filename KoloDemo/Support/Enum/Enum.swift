//
//  Enum.swift
//  KoloDemo
//
//  Created by Shailendra on 14/07/22.
//

import Foundation

enum Weekday: String {
    case monday, tuesday, wednesday, thursday, friday, saturday, sunday
}

enum SearchDirection {
    case next
    case previous
    
    var calendarSearchDirection: Calendar.SearchDirection {
        switch self {
        case .next:
            return .forward
        case .previous:
            return .backward
        }
    }
}
