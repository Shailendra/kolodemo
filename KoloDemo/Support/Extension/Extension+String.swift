//
//  Extension.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation

extension String{
    static let kEmpty                  = ""
    static let kOK                     = "OK"
    static let kInternetConnectionMsg  = "Please check your mobile data or wifi internet connectivity"
    static let kSomethingWrong         = "Something went wrong"
    static let kNoCharacterFound       = "No Character Found"
    static let kNoComicsFound          = "No Comics Found"
    
    
}
