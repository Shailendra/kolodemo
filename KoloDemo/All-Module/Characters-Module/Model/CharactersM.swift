//
//  CharactersM.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation
struct CharCodable : Codable {
    let code     : Int?
    let status   : String?
    let charData : CharData?
    
    enum CodingKeys: String, CodingKey {
        case code     = "code"
        case status   = "status"
        case charData = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values    = try decoder.container(keyedBy: CodingKeys.self)
        self.code     = try values.decodeIfPresent(Int.self, forKey: .code)          ?? 0
        self.status   = try values.decodeIfPresent(String.self, forKey: .status)     ?? .kEmpty
        self.charData = try values.decodeIfPresent(CharData.self, forKey: .charData)
    }
}

struct CharData : Codable {
    let offset  : Int?
    let limit   : Int?
    let total   : Int?
    let count   : Int?
    let results : [CharResults]?
    
    enum CodingKeys: String, CodingKey {
        
        case offset  = "offset"
        case limit   = "limit"
        case total   = "total"
        case count   = "count"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values   = try decoder.container(keyedBy: CodingKeys.self)
        self.offset  = try values.decodeIfPresent(Int.self, forKey: .offset) ?? 0
        self.limit   = try values.decodeIfPresent(Int.self, forKey: .limit)  ?? 0
        self.total   = try values.decodeIfPresent(Int.self, forKey: .total)  ?? 0
        self.count   = try values.decodeIfPresent(Int.self, forKey: .count)  ?? 0
        self.results = try values.decodeIfPresent([CharResults].self, forKey: .results)
    }
}

struct CharResults : Codable {
    
    let name      : String?
    let thumbnail : Thumbnail?
    
    enum CodingKeys: String, CodingKey {
        
        case name      = "name"
        case thumbnail = "thumbnail"
    }
    
    init(from decoder: Decoder) throws {
        let values     = try decoder.container(keyedBy: CodingKeys.self)
        self.name      = try values.decodeIfPresent(String.self, forKey: .name)         ?? .kEmpty
        self.thumbnail = try values.decodeIfPresent(Thumbnail.self, forKey: .thumbnail)
    }
}
