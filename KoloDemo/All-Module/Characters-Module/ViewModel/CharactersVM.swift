//
//  CharactersVM.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation

class CharactersVM : NSObject{

    var API      : WebService!
    var success  : ((CharCodable)-> ())? = nil
    var error    : ((String)-> ())? = nil
    var name     : String = .kEmpty
    var offset   = 0, limit = 50
    
    override init() {
        super.init()
        self.API      = WebService()
        self.callCharactersWebAPI()
    }
}

extension CharactersVM {
    
    
    internal func callCharactersWebAPI() {
        Spinner.start()
        let ts = NSDate().timeIntervalSince1970.description
        let hash = "\(ts)\(kPrivateKey)\(kPublicKey)".MD5
        var url : String = .kEmpty
        if self.name == .kEmpty{
            url  = Environment.kBaseURL + "characters?ts=\(ts)&apikey=\(kPublicKey)&hash=\(hash)&offset=\(self.offset)&limit=\(self.limit)"
        }else{
            url  = Environment.kBaseURL + "characters?name=\(self.name.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)!)&ts=\(ts)&apikey=\(kPublicKey)&hash=\(hash)&offset=\(self.offset)&limit=\(self.limit)"
        }
        print("url====\(url)")
        self.API.callWebAPI(url,param: nil) { [self] data, code in
            if code == 200{
                Spinner.stop()
                do {
                    let result = try JSONDecoder().decode(CharCodable.self, from: data)
                    self.success!(result)
                }catch{
                    self.error!(.kSomethingWrong)
                }
            }else{
                Spinner.stop()
                self.error!(.kSomethingWrong)
            }
        } failure: { errormsg in
            Spinner.stop()
            self.error!(errormsg)
        }
    }
}
