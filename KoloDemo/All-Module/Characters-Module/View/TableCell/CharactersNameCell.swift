//
//  CharactersNameCell.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import UIKit

class CharactersNameCell: UITableViewCell {

    //MARK: - PROPERTIES
    @IBOutlet var characterNameLabel : UILabel!
    
    class var identifier : String {
        return String(describing: self)
    }
    class var nib : UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
