//
//  GridCell.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import UIKit

class GridCell: UICollectionViewCell {

    //MARK: - PROPERTIES
    @IBOutlet var characterNameLabel : UILabel!
    @IBOutlet var characterImageView : UIImageView!
    
    class var identifier : String {
        return String(describing: self)
    }
    class var nib : UINib{
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.characterImageView.layer.cornerRadius = 10
        self.characterImageView.layer.borderWidth  = 3.0
    }
}
