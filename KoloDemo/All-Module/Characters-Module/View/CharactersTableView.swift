//
//  CharactersTableView.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation
import UIKit

//===================================================================
//MARK: - UITableViewDelegate, UITableViewDataSource
//===================================================================
extension ViewController : UITableViewDelegate, UITableViewDataSource{
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characterNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CharactersNameCell.identifier) as! CharactersNameCell
        if self.characterNameArray.count>0{
            cell.characterNameLabel.text = "\(self.characterNameArray[indexPath.row])"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.historyView.isHidden = true
        self.characterArray.removeAll()
        self.viewModel.name  = "\(self.characterNameArray[indexPath.row])"
        self.viewModel.callCharactersWebAPI()
    }
}
