//
//  ViewController.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import UIKit


class ViewController: BaseVC {

    //MARK: - PROPERITES
    @IBOutlet weak var historyView        : UIView!
    @IBOutlet weak var searchTableView    : UITableView!
    @IBOutlet weak var gridCollectionView : UICollectionView!
    @IBOutlet weak var characterSearchBar : UISearchBar!
    open var numberOfItemsInRow           = 4
    open var minimumSpacing               = 5
    open var edgeInsetPadding             = 5
    var API                               = WebService()
    var viewModel                         : CharactersVM!
    var characterArray                    = [CharResults]()
    var model : CharCodable?              = nil
    let refreshControl                    = UIRefreshControl()
    var characterNameArray                : [String] = []
    
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.openDatabse()
        self.callToViewModelForUIUpdate()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.gridCollectionView.addSubview(refreshControl)
    }
    
    //MARK: - REFESH THE COLLECTION VIEW
    @objc func refresh(_ sender: AnyObject) {
        self.refreshControl.endRefreshing()
        self.viewModel.offset = 0
        self.viewModel.name   = .kEmpty
        self.viewModel.callCharactersWebAPI()
    }
    
    //MARK: - REGISTER THE CILLECTIONVIEW CALL AND TABLE VIEW CELL
    private func registerCell(){
        self.searchTableView.register(CharactersNameCell.nib, forCellReuseIdentifier: CharactersNameCell.identifier)
        self.gridCollectionView.register(GridCell.nib, forCellWithReuseIdentifier: GridCell.identifier)
    }
    
    //MARK: - CELL THE WEB SERVICE
    func callToViewModelForUIUpdate(){
        self.viewModel = CharactersVM()
        self.viewModel.success = { (modeldata) in
            DispatchQueue.main.async {
                if (modeldata.charData?.results!)?.count ?? 0 > 0{
                    self.model = modeldata
                    for modelData in (modeldata.charData?.results!)! {
                        self.characterArray.append(modelData)
                    }
                }
                else{
                    self.characterArray.removeAll()
                    self.ShowTheAlert(msg: .kNoCharacterFound)
                }
                self.gridCollectionView.reloadData()
            }
        }
        self.viewModel.error = { (errorMsg) in
            DispatchQueue.main.async {
                self.ShowTheAlert(msg: errorMsg)
            }
        }
    }
    //MARK: - RESET BUTTON ACTION CALL
    @IBAction func ResetButtonAction(){
        self.resetTheCharecterList()
    }
    
    //MARK: - RESET THE CHARECTER LIST
    func resetTheCharecterList(){
        self.historyView.isHidden = true
        self.characterArray.removeAll()
        self.viewModel.name = .kEmpty
        self.viewModel.offset = 0
        self.viewModel.callCharactersWebAPI()
    }
}

//=====================================
// MARK: - UISearchBarDelegate
//=====================================
extension ViewController : UISearchBarDelegate{
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if self.fetchData().count>0{
            self.searchTableView.reloadData()
            self.historyView.isHidden = false
        }
        return true
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = .kEmpty
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !searchBar.text!.isEmpty{
            self.historyView.isHidden = true
            self.characterArray.removeAll()
            self.viewModel.name = searchBar.text!
            self.viewModel.callCharactersWebAPI()
            self.saveData(searchValue: searchBar.text!)
        }
        searchBar.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == .kEmpty {
            searchBar.text = .kEmpty
        }
    }
}

//=====================================
// MARK: - UISearchBarDelegate
//=====================================
extension ViewController : UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView != self.searchTableView{
            if self.model != nil{
                if  self.viewModel.offset < self.model?.charData?.total ?? 0 {
                    self.viewModel.offset += 50
                    self.viewModel.callCharactersWebAPI()
                }
            }
        }
    }
}
