//
//  CharactersCollectionView.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation
import UIKit
import SDWebImage

//===============================================================
//MARK: - UICollectionViewDelegate,UICollectionViewDataSource
//===============================================================
extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  self.characterArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GridCell.identifier, for: indexPath) as! GridCell
        DispatchQueue.main.async {
            cell.characterNameLabel.text = self.characterArray[indexPath.item].name?.uppercased()
            var urlStr = self.characterArray[indexPath.item].thumbnail?.path ?? .kEmpty
            urlStr = urlStr + "." + "\(self.characterArray[indexPath.item].thumbnail?.extensions ?? .kEmpty)"
            cell.characterImageView.sd_setImage(with: URL(string:urlStr),placeholderImage: UIImage(named: "ic-no-Image"))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (Int(UIScreen.main.bounds.size.width) - (self.numberOfItemsInRow - 1) * self.minimumSpacing - self.edgeInsetPadding) / self.numberOfItemsInRow
        return CGSize(width: width, height: width)
    }
}
//=================================================
//MARK: - UICollectionViewDelegateFlowLayout
//=================================================
extension ViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        self.edgeInsetPadding = Int(inset.left + inset.right)
        return inset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(self.minimumSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(self.minimumSpacing)
    }
}
