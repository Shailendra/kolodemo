//
//  SaveInCoreData.swift
//  KoloDemo
//
//  Created by Shailendra on 14/07/22.
//

import Foundation
import CoreData


extension ViewController{
    
    func openDatabse(){
        self.context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Character", in: context)
        self.characterMO = NSManagedObject(entity: entity!, insertInto: context)
    }
    
    func saveData(searchValue : String){
        self.characterMO.setValue(searchValue, forKey: "name")
        do {
            try self.context.save()
        } catch {
            print("Storing data Failed")
        }
    }
    
    func fetchData()->[String] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Character")
        request.returnsObjectsAsFaults = false
        do {
            self.characterNameArray.removeAll()
            let result = try self.context.fetch(request)
            for data in result as! [NSManagedObject] {
                if let characterName = data.value(forKey: "name") as? String{
                    self.characterNameArray.append(characterName)
                }
            }
            return self.characterNameArray
        } catch {
            print("Fetching data Failed")
        }
        return []
    }
}

