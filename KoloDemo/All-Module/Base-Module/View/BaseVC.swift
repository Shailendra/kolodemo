//
//  BaseVC.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import UIKit
import CoreData


class BaseVC: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context     : NSManagedObjectContext!
    var characterMO = NSManagedObject()
    
    //MARK: -  VIEW CONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    //MARK: - DISMISS TO VIEW CONTROLLER
    func DISMISS(_ animated : Bool = true){
        self.dismiss(animated: animated, completion: nil)
    }
    //MARK: - POP TO VIEW CONTROLLER
    func POP(_ animated : Bool = true){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: - POP TO ROOT  VIEW CONTROLLER
    func POP_TO_ROOT(_ animated : Bool = true){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - DISPLAY ALERT FOR MESSAGE
    public func ShowTheAlert(msg:String){
        let otherAlert = UIAlertController(title: .kEmpty, message: msg, preferredStyle: UIAlertController.Style.alert)
        let Okay = UIAlertAction(title: .kOK, style: UIAlertAction.Style.default) { _ in
        }
        otherAlert.addAction(Okay)
        self.present(otherAlert, animated: true, completion: nil)
    }
}

