//
//  ComicsM.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import Foundation

struct ComicsCodable : Codable {
   
    let code   : Int?
    let status : String?
    let DaTa   : DATA?

    enum CodingKeys: String, CodingKey {
        
        case code   = "code"
        case status = "status"
        case DaTa   = "data"
    }

    init(from decoder: Decoder) throws {
        let values  = try decoder.container(keyedBy: CodingKeys.self)
        self.code   = try values.decodeIfPresent(Int.self, forKey: .code)      ?? 0
        self.status = try values.decodeIfPresent(String.self, forKey: .status) ?? .kEmpty
        self.DaTa   = try values.decodeIfPresent(DATA.self, forKey: .DaTa)
    }
}

struct DATA : Codable {
    let offset  : Int?
    let limit   : Int?
    let total   : Int?
    let count   : Int?
    let results : [Results]?
    
    enum CodingKeys: String, CodingKey {
        
        case offset  = "offset"
        case limit   = "limit"
        case total   = "total"
        case count   = "count"
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values   = try decoder.container(keyedBy: CodingKeys.self)
        self.offset  = try values.decodeIfPresent(Int.self, forKey: .offset)        ?? 0
        self.limit   = try values.decodeIfPresent(Int.self, forKey: .limit)         ?? 0
        self.total   = try values.decodeIfPresent(Int.self, forKey: .total)         ?? 0
        self.count   = try values.decodeIfPresent(Int.self, forKey: .count)         ?? 0
        self.results = try values.decodeIfPresent([Results].self, forKey: .results)
    }
}

struct Results : Codable {

    let title     : String?
    let thumbnail : Thumbnail?

    enum CodingKeys: String, CodingKey {

        case title     = "title"
        case thumbnail = "thumbnail"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try values.decodeIfPresent(String.self, forKey: .title)            ?? .kEmpty
        self.thumbnail = try values.decodeIfPresent(Thumbnail.self, forKey: .thumbnail)
    }
}

struct Thumbnail : Codable {
   
    let path       : String?
    let extensions : String?
    
    enum CodingKeys: String, CodingKey {
        
        case path = "path"
        case extensions = "extension"
    }
    
    init(from decoder: Decoder) throws {
        let values      = try decoder.container(keyedBy: CodingKeys.self)
        self.path       = try values.decodeIfPresent(String.self, forKey: .path)        ?? .kEmpty
        self.extensions = try values.decodeIfPresent(String.self, forKey: .extensions)  ?? .kEmpty
    }
}
