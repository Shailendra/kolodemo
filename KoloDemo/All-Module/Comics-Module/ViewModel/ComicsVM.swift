//
//  ComicsVM.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//
import Foundation

class ComicsVM : NSObject{
    
    var API      : WebService!
    var success  : ((ComicsCodable)-> ())? = nil
    var error    : ((String)-> ())? = nil
    var isFilter : Bool = false
    var offset   = 0, limit = 50
    var modified : String = .kEmpty
    
    override init() {
        super.init()
        self.API      = WebService()
        self.callComicsWebAPI()
    }
}

extension ComicsVM {
    
    internal func callComicsWebAPI() {
        Spinner.start()
        let ts = NSDate().timeIntervalSince1970.description
        let hash = "\(ts)\(kPrivateKey)\(kPublicKey)".MD5
        var url : String = .kEmpty
        if self.isFilter == false{  //MARK: - THE API WILL CALL WHEN FILTER IS NOT APPLICABLE
            url  =  Environment.kBaseURL + "comics?ts=\(ts)&apikey=\(kPublicKey)&hash=\(hash)&offset=\(self.offset)&limit=\(self.limit)"
        }else{  //MARK: - THE API WILL CALL WHEN FILTER IS  APPLICABLE
            url  = Environment.kBaseURL + "comics?ts=\(ts)&apikey=\(kPublicKey)&hash=\(hash)&offset=\(self.offset)&limit=\(self.limit)&modifiedSince=\(self.modified)"
        }
        print("url====\(url)")
        self.API.callWebAPI(url,param: nil) { [self] data, code in
            if code == 200{
                Spinner.stop()
                do {
                    let result = try JSONDecoder().decode(ComicsCodable.self, from: data)
                    self.success!(result)
                }catch{
                    self.error!(.kSomethingWrong)
                }
            }else{
                Spinner.stop()
                self.error!(.kSomethingWrong)
            }
        } failure: { errormsg in
            Spinner.stop()
            self.error!(errormsg)
        }
    }
}
