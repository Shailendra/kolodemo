//
//  ComicsVC.swift
//  KoloDemo
//
//  Created by Shailendra on 12/07/22.
//

import UIKit

class ComicsVC: BaseVC {

    //MARK: - PROPERITES
    var API                = WebService()
    var viewModel          : ComicsVM!
  
    @IBOutlet weak var gridCollectionView : UICollectionView!
    open var numberOfItemsInRow = 4
    open var minimumSpacing     = 5
    open var edgeInsetPadding   = 5
    var  comicsArray            = [Results]()
    var model : ComicsCodable?  = nil
    let refreshControl          = UIRefreshControl()
    
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCell()
        self.callToViewModelForUIUpdate()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        self.gridCollectionView.addSubview(refreshControl)
    }
    
    //MARK: - REFESH THE COLLECTION VIEW
    @objc func refresh(_ sender: AnyObject) {
        self.refreshControl.endRefreshing()
        self.viewModel.offset   = 0
        self.viewModel.isFilter = false
        self.viewModel.callComicsWebAPI()
    }
    
    //MARK: - REGISTER THE CILLECTIONVIEW CALL
    private func registerCell(){
        self.gridCollectionView.register(GridCell.nib, forCellWithReuseIdentifier: GridCell.identifier)
    }
    
    //MARK: - CELL THE WEB SERVICE
    func callToViewModelForUIUpdate(){
        self.viewModel = ComicsVM()
        self.viewModel.success  =  { (modeldata) in
            DispatchQueue.main.async {
                if (modeldata.DaTa?.results!)?.count ?? 0 > 0{
                    self.model = modeldata
                    for modelData in (modeldata.DaTa?.results!)! {
                        self.comicsArray.append(modelData)
                    }
                }
                else{
                    if self.comicsArray.count == 0 {
                        self.comicsArray.removeAll()
                        self.ShowTheAlert(msg: .kNoComicsFound)
                    }
                }
                self.gridCollectionView.reloadData()
            }
        }
        
        self.viewModel.error = { (errorMsg) in
            DispatchQueue.main.async {
                self.ShowTheAlert(msg: errorMsg)
            }
        }
    }
    
    //MARK: - LOAD REMAIN VIEW
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.isFilter = false
    }
    
    //MARK: - FILTER BUTTON ACTION
    @IBAction private func filterAction(){
        
        let optionFilter = UIAlertController(title: nil, message: "Choose filter Option", preferredStyle: .actionSheet)
        let thisWeekAction = UIAlertAction(title: "Released this week", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.viewModel.modified  = "\(Date.today())".components(separatedBy: " ")[0]
            self.hideViewModel()
        })
        
        let lastWeekAction = UIAlertAction(title: "Released last week", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.viewModel.modified  =  "\(Date.today().previous(.monday))".components(separatedBy: " ")[0]
            self.hideViewModel()
        })
        
        let nextWeekAction = UIAlertAction(title: "Releasing next week", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.viewModel.modified  = "\(Date.today().next(.monday))".components(separatedBy: " ")[0]
            self.hideViewModel()
        })
        
        let thisMonthAction = UIAlertAction(title: "Release this month", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.viewModel.modified = "1-\(Calendar.current.component(.month, from: Date()))-\(Calendar.current.component(.year, from: Date()))"
            self.hideViewModel()
        })
        
        let resetAction = UIAlertAction(title: "Reset", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.hideViewModel(isFilter: false)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in})
        optionFilter.addAction(thisWeekAction)
        optionFilter.addAction(lastWeekAction)
        optionFilter.addAction(nextWeekAction)
        optionFilter.addAction(thisMonthAction)
        optionFilter.addAction(resetAction)
        optionFilter.addAction(cancelAction)
        self.present(optionFilter, animated: true, completion: nil)
    }
    
    func hideViewModel(isFilter : Bool = true){
        self.comicsArray.removeAll()
        self.viewModel.offset = 0
        self.viewModel.isFilter = isFilter
        self.viewModel.callComicsWebAPI()
    }
}

//=====================================
// MARK: - UISearchBarDelegate
//=====================================
extension ComicsVC : UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.model != nil{
            if  self.viewModel.offset < self.model?.DaTa?.total ?? 0 {
                self.viewModel.offset += 50
                self.viewModel.callComicsWebAPI()
            }
        }
    }
}
